'use strict';

export default {
    'User selects a valid Enterprise to log into': (browser) => {
        const config = require('config');

        browser
            .url(config.enterprise.url)
            .waitForElementVisible('#code', 1000)
            .clearValue('input[name=code]')
            .setValue('input[name=code]', 'avizia')
            .click('button[type=submit]')
            .waitForElementPresent('#email', 2000);

    },

    'User logs into the Enterprise site': (browser) => {
        browser
            .clearValue('input[name=email')
            .clearValue('input[name=password]')
            .setValue('input[name=email]', 'blackwell@avizia.com')
            .setValue('input[name=password]', 'Avizia2016')
            .click('button[type=submit]')
            .waitForElementVisible('#headerDropdown', 3000)
            .pause(3000)
            .end();
    }
};
