'use strict';

const helper = require('../helper.js');

export default {
    before(browser) {
        helper.loginEnterprise(browser);
    },

    after(browser) {
        browser.end();
    },

    'User logs out from the Enterprise site': (browser) => {
        browser
            .click('#headerDropdown')
            .waitForElementVisible('#headerLogout', 1000)
            .click('#headerLogout')
            .waitForElementPresent('#code', 1000)
            .assert.urlContains('/enterprise');
    }
}